import unittest
from fizzbuzz import fizz_buzz

class TestFizzBuzz(unittest.TestCase):
    def test_3_is_fizz(self):
        self.assertEqual(fizz_buzz(3), 'Fizz')
        
    def test_5_is_buzz(self):
        self.assertEqual(fizz_buzz(5), 'Buzz')
        
    def test_15_is_fizzbuzz(self):
        self.assertEqual(fizz_buzz(15), 'FizzBuzz')
        
    def test_7_is_pop(self):
        self.assertEqual(fizz_buzz(7), 'Pop')
        
    def test_21_is_fizzpop(self):
        self.assertEqual(fizz_buzz(21), 'FizzPop')
        
    def test_is_8_number(self):
        self.assertEqual(fizz_buzz(8), 8)
        
if __name__ == '__main__':
    unittest.main()